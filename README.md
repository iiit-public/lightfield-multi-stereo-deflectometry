#  Multi-stereo deflectometry with a light field camera

This is Python code for the deflectometric measurement of specular surfaces using a light field-based
regularization approach.


## Description
Specular surfaces can be measured contactless by means of deflectometric measuring methods. 
A camera observes a reference monitor in the reflection of the surface. By suitable coding of 
the monitor pixels, a monitor pixel can be assigned to each camera pixel.

The shape of the surface can be inferred from the deflectometric measurement.

However, the solution is ambiguous, so further regularization is necessary.

Here, a light field camera is used and interpreted as a multi-camera array in order to resolve the ambiguity 
of the deflectometric measurement by means of a multi-stereo approach.


## License and Usage

This software is licensed under the GNU GPLv3 license (see below).

If you use this software in your scientific research, 
please cite [our paper](https://doi.org/10.1515/teme-2018-0042):

    @article{Uhlig2018,
        author  = {David Uhlig and Michael Heizmann},
        title   = {Multi-Stereo-Deflektometrie mit einer Lichtfeldkamera / Multi-stereo deflectometry with a light-field camera},
        journal = {tm - Technisches Messen},
        number  = {s1},
        volume  = {85},
        year    = {2018},
        pages   = {s59--s65}
    }



## How to use
Have a look at ``lightfield_multi_stereo_deflectometry.py``.


## Code
#### Main function to be used:
```
point_cloud, normals, disparity = calculate_surface(registration_xm: ndarray,
                                                    registration_ym: ndarray,
                                                    mask: ndarray,
                                                    camera_parameters: dict,
                                                    monitor_parameters: dict,
                                                    method: Optional[str] = 'brents_method',
                                                    search_start: Optional[float] = 0.1,
                                                    search_end: Optional[float] = 1.0,
                                                    max_iterations: Optional[int] = 100,
                                                    show_progress: Optional[bool] = False) 
```
#### Input:
* ``registration_xm``: 4D-array of observed monitor x-coordinates. The first two dimensions need to be the angular 
  dimension (the index of the sub image of the light field), the last two dimensions represent the spatial resolution.
* ``registration_ym``: 4D-array of observed monitor y-coordinates.
* ``mask``: 4D-array of masked out coordinates.
* ``camera_parameters``: Dictionary of the intrinsics and transformations between sub-cameras.
* ``monitor_parameters``: Dictionary of the transformation to world coordinates and resolution of monitor.
* ``method``: Optimization method: 'brents_method', 'golden_section_search', 'range_search'.
* ``search_start``: Minimum distance of search intervall.
* ``search_end``: Maximum distance of search intervall.
* ``max_iterations``: Maximum number of iterations in optimization.
* ``show_progress``: Display optimization progress as surface plot.

#### Output:
* ``point_cloud``: Point cloud representing the surface with minimal normal-disparity.
* ``normals``: Corresponding Surface Normals.
* ``disparity``: Normal-Disparity after optimization. 


## License

Copyright (C) 2018-2021  David Uhlig

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
