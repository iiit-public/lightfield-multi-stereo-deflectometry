# Copyright (C) 2021  David Uhlig
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from time import time

import matplotlib.pyplot as plt

import numpy as np
import multi_view_regularization


#%%
def main():
    # Get registration data
    # file_path = '\Example Data\plane22.5_camMon45/'
    file_path = '\Example Data\sphere_radius0.02_distance0.2/'

    # Load registration data
    print('Load registration data...')
    # x-coordinates of the reference monitor seen by the light field camera
    lf_registration_xm = np.load(file_path + 'lf_registration_xm.npy')
    # y-coordinates of the reference monitor
    lf_registration_ym = np.load(file_path + 'lf_registration_ym.npy')
    # mask out invalid pixels (e.g. pixel that don't see the surface)
    lf_mask = np.load(file_path + 'lf_mask.npy')

    # Load Calibration data (use flat to get correct dictionary data)
    print('Load calibration data...')
    camera_parameters = (np.load(file_path + 'camera_parameters.npy', allow_pickle=True)).flat[0]
    monitor_parameters = (np.load(file_path + 'monitor_parameters.npy', allow_pickle=True)).flat[0]

    # Calculate point cloud
    tt = time()
    point_cloud, normals, disparity = multi_view_regularization.calculate_surface(registration_xm=lf_registration_xm,
                                                                                  registration_ym=lf_registration_ym,
                                                                                  mask=lf_mask.astype(dtype=bool),
                                                                                  camera_parameters=camera_parameters,
                                                                                  monitor_parameters=monitor_parameters,
                                                                                  method='brents_method',
                                                                                  search_start=0.01,
                                                                                  search_end=1,
                                                                                  max_iterations=100,
                                                                                  show_progress=True)
    tt = time()-tt
    print(f'Processing time: {tt}')

    # Show result
    from mpl_toolkits.mplot3d import Axes3D
    fig = plt.figure('Surface')
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(xs=point_cloud[0, ::1],
               ys=point_cloud[1, ::1],
               zs=point_cloud[2, ::1],
               c='b',  # point_cloud[2, ::10],
               s=0.1,
               depthshade=False)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.show()


if __name__ == "__main__":
    main()
