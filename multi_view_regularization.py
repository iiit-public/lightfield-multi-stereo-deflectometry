# Copyright (C) 2021  David Uhlig
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from typing import Optional, Union, Tuple

import matplotlib.pyplot as plt
import numpy as np
from numpy.core.multiarray import ndarray
from scipy import interpolate, ndimage
from skimage.morphology import disk


#%%
def calculate_surface(registration_xm: ndarray,
                      registration_ym: ndarray,
                      mask: ndarray,
                      camera_parameters: dict,
                      monitor_parameters: dict,
                      method: Optional[str] = 'brents_method',
                      search_start: Optional[float] = 0.1,
                      search_end: Optional[float] = 1.0,
                      max_iterations: Optional[int] = 100,
                      show_progress: Optional[bool] = False) -> Tuple[ndarray, ndarray, ndarray]:
    """Calculates surface points by minimizing the normal disparity between multiple observations.
    The surfaces is reconstructed for the center view only

    See Also:
        Uhlig, David; Heizmann, Michael (2018): Multi-Stereo-Deflektometrie mit einer
        Lichtfeldkamera / Multi-stereo deflectometry with a light-field camera.
        In: tm - Technisches Messen 85 (s1), s59-s65. DOI: 10.1515/teme-2018-0042.

    Args:
        registration_xm: 4D-array of observed monitor x-coordinates.
        registration_ym: 4D-array of observed monitor y-coordinates.
        mask: 4D-array of masked out coordinates.
        camera_parameters: Intrinsics and transformations between sub-cameras.
        monitor_parameters: Transformation to world coordinates and resolution of monitor.
        method: Optimization method: 'brents_method', 'golden_section_search', 'range_search'.
        search_start: Minimum distance of search intervall.
        search_end: Maximum distance of search intervall.
        max_iterations: Maximum number of iterations in optimization.
        show_progress: Display optimization progress as surface plot.

    Returns:
        Point cloud representing the surface with minimal normal-disparity.
    """

    # get dimensions of data
    v_dim, u_dim, y_dim, x_dim = registration_xm.shape[0:4]
    center_idx = (int(np.floor(v_dim/2)), int(np.floor(u_dim/2)))

    # Erode registration mask to mask out invalid interpolation points
    # Remove neighbors of pixel points with at least one invalid neighbor
    for v in range(v_dim):
        for u in range(u_dim):
            # Center image mask doesn't need to be eroded, because there
            # is no interpolation necessary.
            # TODO: erode anyway? there are some problems at mask borders.. maybe rendering artifacts......
            if v != center_idx[0] and u != center_idx[1]:
                mask[v, u, :, :] = ndimage.morphology.binary_erosion(np.squeeze(mask[v, u, :, :]),
                                                                     structure=disk(4))
            else:
                mask[v, u, :, :] = ndimage.morphology.binary_erosion(np.squeeze(mask[v, u, :, :]),
                                                                     structure=disk(4))
            # if not ((u == 1 and v == 1) or (u == 11 and v == 1) or (u == 1 and v == 11)
            #         or (u == 11 and v == 11) or (u == 6 and v == 6)):
            #     mask[v, u, :, :] = False

    # Initialize center coordinates
    # Select only coordinates with valid registration data
    c_pix_y, c_pix_x = np.where(mask[center_idx[0], center_idx[1], :, :])
    pixel_center = np.asarray((c_pix_x, c_pix_y))

    print('Calculate surface by minimizing normal-disparity:')

    # select optimization method
    method = method.lower()
    if method == 'range_search':
        # Start depth estimation by searching range of depth values

        # Parameters for search
        search_range = np.linspace(search_start, search_end, max_iterations)

        # Create cell arrays for subviews
        # normal_field = zeros(length(search_range),3,size(pixel_center,2));
        points_center_all = np.zeros((search_range.size, 3, pixel_center.shape[1]))
        cost_volume = -1*np.ones((pixel_center.shape[1], search_range.size))

        # Start search
        print('Processing...')
        print('Calculate cost volume...')

        for it in range(search_range.size):
            print(f'Iteration {it + 1}')

            # Get z value for current search
            z_points = search_range[it]*np.ones(pixel_center.shape[1])

            # 3D points
            points_center = pixel_to_camera_coordinates(pixel_center,
                                                        z_points,
                                                        camera_parameters['intrinsics'][center_idx[0], center_idx[1]])

            # Get disparity value for every point
            normal_disparity = calculate_normal_disparity(points_center,
                                                          registration_xm,
                                                          registration_ym,
                                                          mask,
                                                          camera_parameters,
                                                          monitor_parameters)

            # All points with corresponding cost values
            points_center_all[it, :, :] = points_center
            cost_volume[:, it] = normal_disparity

            if show_progress is True:
                show_progress_plot(points_center)

        print('Refine point cloud')
        point_cloud = refine_point_cloud(points_center_all, cost_volume)

    elif method == 'golden_section_search':
        # Golden section search in defined range

        # Parameters
        tol = 1e-6  # accuracy value
        tau = (np.sqrt(5) - 1)/2  # golden proportion coefficient, around 0.618

        # Initialize start values
        continue_search = np.ones(pixel_center.shape[1], dtype=bool)
        new_is_1 = np.zeros(continue_search.shape, dtype=bool)
        z_out = np.zeros(continue_search.shape)

        a = search_start*np.ones(pixel_center.shape[1])
        b = search_end*np.ones(pixel_center.shape[1])
        z1 = a + (1 - tau)*(b - a)
        z2 = a + tau*(b - a)

        # Start search at borders of interval
        # Project center pixels to center-view coordinates and calculate costs

        # lower border
        points_world = pixel_to_camera_coordinates(pixel_center,
                                                   z1,
                                                   camera_parameters['intrinsics'][center_idx[0], center_idx[1]])
        cost_z1 = calculate_normal_disparity(points_world,
                                             registration_xm,
                                             registration_ym,
                                             mask,
                                             camera_parameters,
                                             monitor_parameters)
        # upper border
        points_world = pixel_to_camera_coordinates(pixel_center,
                                                   z2,
                                                   camera_parameters['intrinsics'][center_idx[0], center_idx[1]])
        cost_z2 = calculate_normal_disparity(points_world,
                                             registration_xm,
                                             registration_ym,
                                             mask,
                                             camera_parameters,
                                             monitor_parameters)

        # Start optimization by iteratively shrinking the search interval
        print('Iteration     Mean f(x)     Mean x')
        for it in range(max_iterations):
            # Check all points separately
            for i in range(cost_z1.size):
                # Only do optimization for points that aren't already optimized
                if continue_search[i]:
                    # Check which has lower disparity
                    if cost_z1[i] < cost_z2[i]:
                        # Set new interval
                        b[i] = z2[i]
                        z2[i] = z1[i]
                        z1[i] = a[i] + (1 - tau)*(b[i] - a[i])
                        cost_z2[i] = cost_z1[i]

                        # New cost estimate at z1
                        new_is_1[i] = True
                    else:
                        # Set new interval
                        a[i] = z1[i]
                        z1[i] = z2[i]
                        z2[i] = a[i] + tau*(b[i] - a[i])
                        cost_z1[i] = cost_z2[i]

                        # New cost estimate at z2
                        new_is_1[i] = False

            # Check if max accuracy is reached. Do not continue search for those
            # points. Only for other points.
            continue_search = np.abs(b - a) > tol
            if np.sum(continue_search) == 0:
                break

            # Set new depth. Decide which depth should be selected. z1 or z2? * equals and for bool
            z_out[new_is_1*continue_search] = z1[new_is_1*continue_search]
            z_out[(~new_is_1)*continue_search] = z2[(~new_is_1)*continue_search]

            # Calc new costs for all points
            points_world = pixel_to_camera_coordinates(pixel_center[:, continue_search],
                                                       z_out[continue_search],
                                                       camera_parameters['intrinsics'][center_idx[0], center_idx[1]])
            cost_new = calculate_normal_disparity(points_world,
                                                  registration_xm,
                                                  registration_ym,
                                                  mask,
                                                  camera_parameters,
                                                  monitor_parameters)

            # Check if costs are for z1 or z2? * equals and for bool
            cost_z1[new_is_1*continue_search] = cost_new[new_is_1[continue_search]]
            cost_z2[(~new_is_1)*continue_search] = cost_new[(~new_is_1[continue_search])]

            if np.sum(continue_search) > 0:
                print(f'{it + 1:{5}}     {((np.mean(cost_z1) + np.mean(cost_z2))/2):{12}.{6}}'
                      f'  {np.mean(z_out):{12}.{8}}')
                if show_progress is True:
                    point_cloud = pixel_to_camera_coordinates(pixel_center,
                                                              z_out,
                                                              camera_parameters['intrinsics'][
                                                                  center_idx[0], center_idx[1]])
                    show_progress_plot(point_cloud)

        # Return value in the middle of the interval
        z_out = (a + b)/2.0

        point_cloud = pixel_to_camera_coordinates(pixel_center,
                                                  z_out,
                                                  camera_parameters['intrinsics'][center_idx[0], center_idx[1]])

    elif method == 'brents_method':
        # Brent's Method:
        # Mix of successive parabolic interpolation and golden section search
        # Based on "Brent, R. P.: Algorithms for minimization without
        # derivatives. 1973: Prentice-Hall, Englewood Cliffs, NJ." and Matlab 1D fminbnd

        # Parameters for algorithm
        tol = 1e-6
        eps = np.finfo(float).eps  # Machine precision
        seps = np.sqrt(eps)
        tau = 0.5*(3.0 - np.sqrt(5.0))  # golden proportion coefficient

        # Compute the start point
        a = search_start*np.ones(pixel_center.shape[1])
        b = search_end*np.ones(pixel_center.shape[1])
        v = a + tau*(b - a)
        w = v.copy()
        xf = v.copy()
        d = np.zeros(a.shape)
        e = np.zeros(a.shape)
        x = xf.copy()

        # Calculate initial cost values
        points_world = pixel_to_camera_coordinates(pixel_center,
                                                   x,
                                                   camera_parameters['intrinsics'][center_idx[0], center_idx[1]])
        fx = calculate_normal_disparity(points_world,
                                        registration_xm,
                                        registration_ym,
                                        mask,
                                        camera_parameters,
                                        monitor_parameters)
        fv = fx.copy()
        fw = fx.copy()
        fu = np.zeros(fx.shape)
        xm = 0.5*(a + b)
        tol1 = seps*np.abs(xf) + tol/3.0
        tol2 = 2.0*tol1

        print('Iteration     Mean f(x)     Mean x    Procedure---------------------------')
        # Start optimization by iteratively shrinking the search interval
        for it in range(max_iterations):
            # Check if max accuracy is reached. Do not continue search for those
            # points. Only for other points.
            continue_search = np.abs(xf - xm) > (tol2 - 0.5*(b - a))
            if np.sum(continue_search) == 0:
                break

            # For logging
            parabola_cnt = 0
            gold_cnt = 0

            # Check all points separately
            for i in range(continue_search.size):
                # Only do optimization for points that aren't already optimized
                if continue_search[i]:
                    golden_search = True
                    # Is a parabolic fit possible?
                    if abs(e[i]) > tol1[i]:
                        # Yes, so fit parabola
                        golden_search = False
                        r = (xf[i] - w[i])*(fx[i] - fv[i])
                        q = (xf[i] - v[i])*(fx[i] - fw[i])
                        p = (xf[i] - v[i])*q - (xf[i] - w[i])*r
                        q = 2.0*(q - r)
                        if q > 0.0:
                            p = -p
                        q = abs(q)
                        r = e[i]
                        e[i] = d[i]

                        # Is the parabola acceptable?
                        if (abs(p) < abs(0.5*q*r)) and (p > q*(a[i] - xf[i])) and (p < q*(b[i] - xf[i])):
                            # Yes, parabolic interpolation step
                            d[i] = p/q
                            x[i] = xf[i] + d[i]
                            parabola_cnt = parabola_cnt + 1

                            # f must not be evaluated too close to ax or bx
                            if ((x[i] - a[i]) < tol2[i]) or ((b[i] - x[i]) < tol2[i]):
                                si = np.sign(xm[i] - xf[i]) + ((xm[i] - xf[i]) == 0)
                                d[i] = tol1[i]*si
                        else:
                            # Not acceptable, must do a golden section step
                            golden_search = True
                    if golden_search is True:
                        # A golden-section step is required
                        if xf[i] >= xm[i]:
                            e[i] = a[i] - xf[i]
                        else:
                            e[i] = b[i] - xf[i]
                        d[i] = tau*e[i]
                        gold_cnt = gold_cnt + 1

                    # The function must not be evaluated too close to xf
                    si = np.sign(d[i]) + (d[i] == 0)
                    x[i] = xf[i] + si*max(abs(d[i]), tol1[i])

            # Calculate new costs
            points_world = pixel_to_camera_coordinates(pixel_center[:, continue_search],
                                                       x[continue_search],
                                                       camera_parameters['intrinsics'][center_idx[0], center_idx[1]])
            fu[continue_search] = calculate_normal_disparity(points_world,
                                                             registration_xm,
                                                             registration_ym,
                                                             mask,
                                                             camera_parameters,
                                                             monitor_parameters)

            # Check all points separately
            for i in range(continue_search.size):
                # Only do optimization for points that aren't already optimized
                if continue_search[i]:
                    # Update a, b, v, w, x
                    if fu[i] <= fx[i]:
                        if x[i] >= xf[i]:
                            a[i] = xf[i]
                        else:
                            b[i] = xf[i]
                        v[i] = w[i]
                        fv[i] = fw[i]
                        w[i] = xf[i]
                        fw[i] = fx[i]
                        xf[i] = x[i]
                        fx[i] = fu[i]
                    else:  # fu > fx
                        if x[i] < xf[i]:
                            a[i] = x[i]
                        else:
                            b[i] = x[i]
                        if (fu[i] <= fw[i]) or (w[i] == xf[i]):
                            v[i] = w[i]
                            fv[i] = fw[i]
                            w[i] = x[i]
                            fw[i] = fu[i]
                        elif (fu[i] <= fv[i]) or (v[i] == xf[i]) or (v[i] == w[i]):
                            v[i] = x[i]
                            fv[i] = fu[i]

            # Update xm, tol1, tol2
            xm[continue_search] = 0.5*(a[continue_search] + b[continue_search])
            tol1[continue_search] = seps*abs(xf[continue_search]) + tol/3.0
            tol2[continue_search] = 2.0*tol1[continue_search]

            # Show progress
            if np.sum(continue_search) > 0:
                print(f'{it + 1:{5}}     {np.mean(fx):{12}.{6}}  {np.mean(xm):{12}.{8}}    '
                      f'{gold_cnt:{6}} x golden {parabola_cnt:{7}} x parabolic')

                if show_progress:
                    point_cloud = pixel_to_camera_coordinates(pixel_center,
                                                              xm,
                                                              camera_parameters['intrinsics'][
                                                                  center_idx[0], center_idx[1]])
                    show_progress_plot(point_cloud)

        point_cloud = pixel_to_camera_coordinates(pixel_center,
                                                  xm,
                                                  camera_parameters['intrinsics'][center_idx[0], center_idx[1]])
    else:
        print('Method doesn´t exist!\n')
        point_cloud = None

    print('Done.\n')

    if show_progress is True:
        plt.close('Surface')
        plt.ioff()
        plt.ioff()

    normal, normal_disparity = calculate_normal_disparity(point_cloud,
                                                          registration_xm,
                                                          registration_ym,
                                                          mask,
                                                          camera_parameters,
                                                          monitor_parameters,
                                                          return_normal=True)

    return point_cloud, normal, normal_disparity


#%%
def calculate_normal_disparity(points_world: ndarray,
                               registration_xm: ndarray,
                               registration_ym: ndarray,
                               mask: ndarray,
                               camera_parameters: dict,
                               monitor_parameters: dict,
                               disparity_measure: Optional[str] = 'fast',
                               return_normal=False) -> Union[ndarray, Tuple[ndarray, ndarray]]:
    """Calculates the disparity of hypothetical surface normals
    at given world points viewed from the cameras.

    Args:
        points_world: (3, N) Array of point coordinates on which the disparity will be calculated.
        registration_xm: 4D-array of observed monitor x-coordinates.
        registration_ym: 4D-array of observed monitor y-coordinates.
        mask: 4D-array of masked out coordinates.
        camera_parameters: Intrinsics and transformations between sub-cameras.
        monitor_parameters: Transformation to world coordinates and resolution of monitor.
        disparity_measure: Method used to calculate the disparity: 'fast', 'slow'.

    Returns:
        Disparity/costs of input points.
    """

    # Return empty array, if there are no points to check
    if points_world.size is 0:
        return []

    # get dimensions
    v_dim, u_dim, y_dim, x_dim = registration_xm.shape[0:4]

    # Initialize variables
    surface_normal = np.zeros((v_dim*u_dim, 3, points_world.shape[1]), dtype=np.float64)
    valid_normals = np.empty((v_dim*u_dim, points_world.shape[1]), dtype=np.float64)

    # Project camera coordinates to pixels of all subApertureImages
    # Check if multiple processor cores should be used
    for kk in range(v_dim*u_dim):
        # Calculate u and v from single index
        vv, uu = np.unravel_index(kk, (v_dim, u_dim))

        # Coordinate Transformations:
        # Transform center coordinates to u,v-camera coordinates (inverse subView2center)

        # points_sub_view = cameraParameters.subView2center{v,u}.rotation.'*points_world ...
        #                  - cameraParameters.subView2center{v,u}.translation;
        points_sub_view = points_world - camera_parameters['subView2center']['translation'][vv, uu][:, np.newaxis]

        # Transform u,v-camera coordinates to u,v-pixel coordinates
        pixel_sub_view = (camera_parameters['intrinsics'][vv, uu][0:2, 0:3]@points_sub_view)/points_sub_view[2, :]

        # Mask out invalid points:
        # Check if projected pixel positions are valid sensor positions
        valid_pixel_idx = np.asarray(((0 <= pixel_sub_view[0, :])*(pixel_sub_view[0, :] <= x_dim - 1))*
                                     ((0 <= pixel_sub_view[1, :])*(pixel_sub_view[1, :] <= y_dim - 1)))

        # Check if projected pixel has valid registration data.
        # Round pixel values. If _mask shows correct registration data,
        # surrounding pixels are correct too, because _mask was eroded.
        valid_registration_idx = mask.flat[
            np.ravel_multi_index((vv,
                                  uu,
                                  np.int32(np.round(pixel_sub_view[1, valid_pixel_idx])),
                                  np.int32(np.round(pixel_sub_view[0, valid_pixel_idx]))),
                                 mask.shape)]

        valid_pixel_idx[valid_pixel_idx] = valid_registration_idx

        # Interpolate:
        # Calculate the corresponding deflectometric registrations and interpolate on sub pixel level
        x = np.arange(0, x_dim)
        y = np.arange(0, y_dim)
        # TODO: speed up by calculating interpolator only once! for all u,v! twice...
        interpolator_xm = interpolate.RegularGridInterpolator((y, x),
                                                              np.squeeze(registration_xm[vv, uu, :, :]),
                                                              method='linear',
                                                              bounds_error=True,
                                                              fill_value=0)
        interpolator_ym = interpolate.RegularGridInterpolator((y, x),
                                                              np.squeeze(registration_ym[vv, uu, :, :]),
                                                              method='linear',
                                                              bounds_error=True,
                                                              fill_value=0)

        registration_xm_interp = interpolator_xm(
            (pixel_sub_view[1, valid_pixel_idx], pixel_sub_view[0, valid_pixel_idx]))
        registration_ym_interp = interpolator_ym(
            (pixel_sub_view[1, valid_pixel_idx], pixel_sub_view[0, valid_pixel_idx]))

        # Monitor points: Mask out & get points
        # Check if registration data is in valid range
        valid_registration_idx = (registration_ym_interp >= 0)* \
                                 (registration_ym_interp <= monitor_parameters['resolution'][1])* \
                                 (registration_xm_interp >= 0)* \
                                 (registration_xm_interp <= monitor_parameters['resolution'][0])

        # Get monitor coordinates
        points_monitor = np.asarray((registration_xm_interp[valid_registration_idx],
                                     registration_ym_interp[valid_registration_idx],
                                     np.zeros(np.sum(valid_registration_idx))
                                     ))*monitor_parameters['pixelPitch']

        # Monitor points: Coordinate transformations
        # Monitor to World coordinates
        points_monitor = monitor_parameters['monitorToWorld']['rotation']@points_monitor + \
                         monitor_parameters['monitorToWorld']['translation'][:, np.newaxis]

        # Center to u,v-camera coordinates:
        # points_monitor = camera_parameters['subView2center']['rotation'][vv, uu]@points_monitor
        points_monitor -= camera_parameters['subView2center']['translation'][vv, uu][:, np.newaxis]

        # Mask out invalid points:
        # Get valid center-view u,v-view pairs
        valid_pixel_idx[valid_pixel_idx] = valid_registration_idx

        # Calculate surface normals:
        # Calculate the surface normals at the test point, using the
        # deflectometric measurements and the known geometrical setup
        normal = (points_monitor - points_sub_view[:, valid_pixel_idx])/np.sqrt(
            np.sum((points_monitor - points_sub_view[:, valid_pixel_idx])**2, axis=0)) - \
                 points_sub_view[:, valid_pixel_idx]/np.sqrt(np.sum(points_sub_view[:, valid_pixel_idx]**2, axis=0))

        # Normalize normal
        normal /= np.sqrt(np.sum(normal**2, axis=0))

        # Transform surface normal to center-view coordinates
        # normal = cameraParameters.subView2center{v,u}.rotation @ normal;

        # Set normals of invalid measurements to zero and keep correct
        # order with respect to center-view points
        temp_normal = np.zeros(points_world.shape)
        temp_normal[:, valid_pixel_idx] = normal

        # Return
        surface_normal[kk, :, :] = temp_normal
        valid_normals[kk, :] = valid_pixel_idx

    # Initialize normal disparity
    normal_disparity = np.inf*np.ones(points_world.shape[1])

    # At least two normals must be valid for disparity estimation.
    num_normal = np.sum(valid_normals, axis=0)  # Should always be >= 1

    # Sum over all normals. Invalid normals are zero
    mean_normal = np.squeeze(np.sum(surface_normal, axis=0, keepdims=True), axis=0)/num_normal

    # If invalid, disparity is inf
    is_valid = np.not_equal(num_normal, 1)  # TODO: Center-view normals should always be valid!?

    if disparity_measure.lower() == 'fast':
        normal_disparity[is_valid] = 1 - np.sum(mean_normal[:, is_valid]*mean_normal[:, is_valid], axis=0)
    elif disparity_measure.lower() == 'slow':
        # Calculate disparity over all normals from all camera-views
        # disp = 1/N*SUM_(i=1...N){ [arccos( <n_i|n_mean> )]^2 }
        normal_disparity[is_valid] = np.mean(
            np.arccos(np.sum(surface_normal[:, :, is_valid]*mean_normal[np.newaxis, :, is_valid], axis=1))**2, axis=0)

    # Heuristic Disparity Measures
    # Divide disparity by mean angle formed by the monitor points,
    # world points and the different camera viewpoints
    # TODO: One angle per point per camera
    # TODO: Weight each disparity separately, not sum by sum of weights!
    # sum_angle =  sum(cat(3, reflection_angle{:}), 3);
    # normal_disparity(is_valid) = normal_disparity(is_valid) ...
    #                              ./sum_angle(is_valid) ...
    #                              .*num_normal(is_valid);

    # Multiply disparity by distance to penalize points too far away
    # TODO: weight??
    normal_disparity = normal_disparity*points_world[2, :]

    if return_normal:
        return mean_normal, normal_disparity
    else:
        return normal_disparity


#%%
def pixel_to_camera_coordinates(pixel_coord: ndarray,
                                z_camera: Union[float, ndarray],
                                camera_intrinsics: dict) -> ndarray:
    """Projects pixel coordinates to camera coordinates using intrinsic camera parameters.

    Args:
        pixel_coord: Array of pixel coordinates.
        z_camera: z-value of  prjocetion point.
        camera_intrinsics: Intrinsic camera matrix. 

    Returns:
        Projected 3D points.
    """

    # fx = camera_intrinsics[0,0]
    # fy = camera_intrinsics[1,1]
    # cx = camera_intrinsics[0,2]
    # cy = camera_intrinsics[1,2]

    pixel_coord = pixel_coord - (camera_intrinsics[0:2, 2])[:, np.newaxis]
    pixel_coord = pixel_coord*z_camera[np.newaxis, :]/np.asarray((camera_intrinsics[0, 0],
                                                                  camera_intrinsics[1, 1]))[:, np.newaxis]
    points_camera_coord = np.concatenate((pixel_coord, z_camera[np.newaxis, :]), axis=0)
    return points_camera_coord


def refine_point_cloud(points_in, cost_volume):
    """Refines point cloud by interpolation of input points on the cost volume.

    Args:
        points_in: Array of 3D points for every interation.
        cost_volume: Cost of each point for every iteration.

    Returns:
        Interpolated point cloud with minimal costs.
    """

    # TODO: make this work!!! np.min doesnt work properly.....
    # Get labels of point cloud
    # Get best depth estimate
    cost_best = np.min(cost_volume, axis=1)
    label_best = np.argmin(cost_volume, axis=1)

    # Get values from second best scores
    cost_plus = np.zeros(label_best.shape)
    cost_neg = np.zeros(label_best.shape)
    for k in range(label_best.size):
        cost_plus[k] = cost_volume[k, min(label_best[k] + 1, cost_volume.shape[1])]
        cost_neg[k] = cost_volume[k, max(label_best[k] - 1, 1)]

    # Mask out labels at top and bottom label border
    mask_b = label_best == 1
    mask_t = label_best == cost_volume.shape[1]
    mask = (~mask_b)*(~mask_t)

    # Calculate parabola vertex
    points_out = np.zeros((3, label_best.size))
    for k in range(label_best.size):
        if mask[k]:
            for i in range(3):
                points_out[i, k] = calc_parabola_vertex(points_in[label_best[k] - 1, i, k],
                                                        cost_neg[k],
                                                        points_in[label_best[k], i, k],
                                                        cost_best[k],
                                                        points_in[label_best[k] + 1, i, k],
                                                        cost_plus[k])

    # Remove if point is nan or inf and return rest
    return points_out[:, ~(np.isnan(np.sum(points_out, 0)) + np.isinf(np.sum(points_out, 0)))]


def calc_parabola_vertex(x_1, y_1, x0, y0, x1, y1):
    """Function to calculate vertex of fitted parabola.
    """
    den = (x_1 - x0)*(x_1 - x1)*(x0 - x1)
    a = (x1*(y0 - y_1) + x0*(y_1 - y1) + x_1*(y1 - y0))/den
    b = (x1*x1*(y_1 - y0) + x0*x0*(y1 - y_1) + x_1*x_1*(y0 - y1))/den
    # c = (x0*x1*(x0 - x1)*y_1 + x1*x_1*(x1 - x_1)*y0 + x_1*x0*(x_1 - x0)*y1) / den
    xv = -b/(2*a)
    # yv = c - b*b / (4*a)
    # return xv, yv
    return xv


#%%
def show_progress_plot(point_cloud):
    if not plt.fignum_exists('Surface'):
        plt.ion()
        fig = plt.figure('Surface')
        ax = fig.add_subplot(111, projection='3d')
        plt.pause(0.025)
        ax.cla()
    ax = plt.gca()
    ax.cla()
    ax.plot(xs=point_cloud[0, :],
            ys=point_cloud[1, :],
            zs=point_cloud[2, :],
            color='b',
            linestyle='None',
            marker='.',
            markersize=1)
    plt.pause(0.002)
